import os

DEFAULTS = {'DB_NAME': 'loadTest',
            'COLLECTION_NAME': 'documents',
            #'CLUSTER_URL': "mongodb://127.0.0.1:27017/",
            'CLUSTER_URL': "mongodb://admin:C1d3siM0ngoAdmin2022@10.40.30.32:27017/",
            #'CLUSTER_URL': "mongodb://admin:C1d3siM0ngoAdmin2022@10.40.30.32:27017/?authSource=admin&readPreference=primary&appname=MongoDB%20Compass&directConnection=true&ssl=false",
            #'CLUSTER_URL': f'mongodb+srv://user:password@something.mongodb.net/sample?retryWrites=true&w=majority',
            'DOCS_PER_BATCH': 100,
            'INSERT_WEIGHT': 1,
            'FIND_WEIGHT': 3,
            'BULK_INSERT_WEIGHT': 1,
            'AGG_PIPE_WEIGHT': 1}
def init_defaults_from_env():
    for key in DEFAULTS.keys():
        value = os.environ.get(key)
        if value:
            DEFAULTS[key] = value


# get the settings from the environment variables
init_defaults_from_env()