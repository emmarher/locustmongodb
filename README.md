# Descripción
Proyecto para relalizar pruebas de carga y estres en el aplicativo web de Simulador de ventilador mecanico.
Se utiliza Locust como herramienta de pruebas.
Las lineas de codigo tienen comentarios que ayudan a entender mejor el funcionamiento del mismo.

# Requisitos 
- python 3.8
- locust 2.8.3
- pandas 1.3.4
- json

---

# Ejecución /  Run

Para ejecutar con GUI, es recomendable asignar la direccion y puerto donde se ejecutan, puesto que en algunos sistemas, si no se define, no se puede acceder.

_Nota: se puede establecer cualquier direccion y puerto disponible_ 
```
locust -f <directorio>/<locust_file>.py --web-host 127.0.0.1 --web-port 3001
```

Los archivos que inician con 'example...' son los reportes  que se generan cuando se ejecuta el comando :

desde command line:
 ```
 locust -f locustfile.py -u 1000 -r 10 -t 1000s --headless --csv=example
 ```
 u --> cantidad de usuarios soimulados
 r --> Rawspown, la creacion de usuarios inicia en 1, y va en aumento de r en r.
 t --> tiempo de ejecución/duración, (s-segundos, m-minutos)
 headless --> es necesario para ejecutar desde command line
 csv --> que guarde reportes con el prefijo escrito.

 Cuando se ejecuta desde el GUI, en la función de _on_stop_, hay valores que se establecen para guardar en listas, posteriormente, cuando se detiene la prueba, se guardan en un dataframe para guardarlos en csv.

Locust permite trabajar de manera distribuida, es importante mencionar que de modo stantalone, trabaha sobre un single core, si se quiere aumentar la capacidad, se tiene que hacer un de un terminal per core, haciendo uso de los master.conf y worker,conf.


Primero se tiene que ejecutar el master.conf con el siguiente comando:

```
    locust --config master.conf --web-host 127.0.0.1 --web-port 3001
```

En otra terminal ejecutar

 ```
    locust --config worker.conf --web-host 127.0.0.1 --web-port 3001
 ```

Es importante establecer el host correcto desde la variable _host_ dentro del archivo __master.conf.__

Lo anterior nos permite entrar al modo GUI y visualizar los workers, asi como los recursos utilizados por cada uno.

## Generación de carga distribuida

Para ahcer esto, es necesario cargar una instancia de Locust en modo master usando la bandera _--master_ y multiples instanciasde workers usando la bander _--worker_. Si los workers no estan en la misma macquina que el master, usa _--master-host_ al punto, esto para agregar la IP/hostname de la maquina que corre el master. 
La instanci master corre la interface Web de Locust, y le dice a los workers cuando inciar/parar, cantidad de usuarios a desplegar. Los workers envian las estadisitcas al master.
Ambos (master-worker)  deben tenre una copia del archivo locust cuando corren de manera distribuida.
 __Master Mode__:
 ```
 locust -f my_locustfile.py --master
 ```


  __Worker Mode__:
 ```
 locust -f my_locustfile.py --worker --master-host=192.168.0.14
 ```

 ```
SVMTest
│-- README.md
│-- CHANGELOG.md
│-- locustfile.py
│-- giignore.gitignore
│-- master.conf
│-- worker.conf
|-- connect_to_db_example.py
│-- mongo_user.py
│-- decimal_codec.py
│-- load_test.py
│-- settings.py
│
└─── ###
│   │-- ##


 ```


# crear entorno virtual
```
python3 -m venv venv
```
Activar entorno virtual

source venv/bin/activate

# Referencia/Docs
https://docs.locust.io/en/stable/running-distributed.html