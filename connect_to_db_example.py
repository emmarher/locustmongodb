import pymongo

host= "mongodb://127.0.0.1:27017/"
client = pymongo.MongoClient(host)
# show databases
print("Databases::", client.list_database_names())
db = client["sample_mflix"]
print("Data::", db.list_collection_names())
col = db['movies']
print('Collection::',col)

# Insert data
new_data = {"title": "Harry Potter 1", "genres":['Drama','Fiction'], "runtime":120, "rated":'R', "year":2000,"directors":['Del toro'], "cast":['Daniel','Emma'], 'type':'movie'}

""" x = col.insert_one(new_data)
print(x.inserted_id) """

y = col.find_one()
print('Un resultado:::',y)

print('\n')
for y in col.find():
    print(y)
print('\n')
# QUERY
query = { "title": 'Harry Potter 1'}
query2 = { "title": {"$gt": "H"} } # title start with letter "H"

doc = col.find(query)
for r in doc:
    print(r)
""" collection = db["python"]
print("Collection", collection)
s = db.movies.find()
print(s) """


"""
    i  insert many documents into collection
    mylist = [
  { "name": "Amy", "address": "Apple st 652"},
  { "name": "Hannah", "address": "Mountain 21"},
  { "name": "Michael", "address": "Valley 345"},
  { "name": "Sandy", "address": "Ocean blvd 2"},
  { "name": "Betty", "address": "Green Grass 1"},
  { "name": "Richard", "address": "Sky st 331"},
  { "name": "Susan", "address": "One way 98"},
  { "name": "Vicky", "address": "Yellow Garden 2"},
  { "name": "Ben", "address": "Park Lane 38"},
  { "name": "William", "address": "Central st 954"},
  { "name": "Chuck", "address": "Main Road 989"},
  { "name": "Viola", "address": "Sideway 1633"}
    ]

    x = mycol.insert_many(mylist)

    #print list of the _id values of the inserted documents:
    print(x.inserted_ids) 
"""

