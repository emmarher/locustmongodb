# -*- coding: utf-8 -*-
"""
Created on Thu Ago  10  12:08:48 2022

@author: emmarher
mongodb://admin:C1d3siM0ngoAdmin2022@10.40.30.32:27017/?authMechanism=DEFAULT


Load Testing MongDB 

Si nombraste el archivo locust como locustfile.py, solo ejecutar locust
Si tiene otro nombre, es con el comando de siguiente:
    locust -f <directorio>/<locust_file>.py

    1) Requests - Muestra el numero de solicitudes
    2) Fails - Muestra el numero de solicitudes fallidas
    3) Median - Que tanto la mediana de total del tiempo de respuesta
    4) Current RPS - Rate per second
    4) Average - Media de tiempo de respuesta

    La cantidad de usuarios por segundo seguira enviando peticiones hasta que lo
    detenga manualmente o se configure un stop con tiempo

     locust -f locustfile.py -u 1 -r 10 -t 1000 --headless --csv=example

    prioriidad a las tareas con el  decoradorr @task(n). donde n es el pesos asignado a la tarea, a mayor el valor de n, mayor es la prioridad para ejecutar esa tarea.
    Las tareas son tomadas al azar, es por ello del los decoradores de tarea con peso.

"""

from locust import HttpUser, task, between
from pymongo import IndexModel, ASCENDING
from settings import DEFAULTS
import random
from mongo_user import MongoUser, mongodb_task

# number of cache entries for queries
NAMES_TO_CACHE = 1000


class TestUser(MongoUser):
    #Set the think time
    wait_time = between(1, 5)
    
    def __init__(self):
        self.name_cache = []


    def generate_new_document(self):
        """
        Generate a new sample document
        """
        document = {
            'first_name': self.faker.first_name(),
            'last_name': self.faker.last_name(),
            'address': self.faker.street_address(),
            'city': self.faker.city(),
            'total_assets': self.faker.pydecimal(min_value=100, max_value=1000, right_digits=2)
        }


    def on_start(self):
        """
        on_start is called when the TaskSet is starting

        Returns
        -------
        None.

        """
        index1 = IndexModel( [ ('first_name',ASCENDING) ] ) # Define an index
        self.collection, _ =  self.ensure_collection('sample',[index1]) # Create the collection

    # Define the first task with the wight of 1
    @mongodb_task(weight=1)
    def insert_document(self):
        #self.collection.insert_one({'first_name': 'John', 'last_name': 'Doe'})
        document = self.generate_new_document()
        # cache the first_name, last_name tuple for queries
        cached_names = (document['first_name'], document['last_name'])
        if len(self.name_cache) < NAMES_TO_CACHE:
            self.name_cache.append(cached_names)
        else:
            if random.randint(0, 9) == 0:
                self.name_cache[random.randint(0, len(self.name_cache) - 1)] = cached_names

        self.collection.insert_one(document)

    # Define the second task the with weight of 3
    @mongodb_task(weight=3)
    def find_document(self):
        self.collection.find_one({'first_name': 'John', 'last_name': 'Doe'})

    @mongodb_task(weight=5)
    def insert_documents_bulk(self):
        self.collection.insert_many(
            [self.generate_new_document() for _ in
             range(int(DEFAULTS['DOCS_PER_BATCH']))])

    

    def on_stop(self):
        """
        Is called when TaskSet is stopping

        Returns
        -------
        None.

        """
        pass


